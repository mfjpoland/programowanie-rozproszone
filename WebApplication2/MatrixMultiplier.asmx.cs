﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Xml.Serialization;

namespace WebApplication2
{
    /// <summary>
    /// Summary description for MatrixMultiplier1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MatrixMultiplier1 : System.Web.Services.WebService
    {
        public MatrixMultiplier1() {
        }
        [XmlElement("Matrix")]
        public MatrixMultiplier matrix { get; set; }
        [WebMethod]
        public MatrixMultiplier setMatrix(string fileName, string streamString)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(streamString);
            MemoryStream stream = new MemoryStream(byteArray);
            string FilePath = Path.Combine(HostingEnvironment.MapPath("~/FileServer/Uploads"), fileName);

            int length = 0;
            using (FileStream writer = new FileStream(FilePath, FileMode.Create))
            {
                int readCount;
                var buffer = new byte[8192];
                while ((readCount = stream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    writer.Write(buffer, 0, readCount);
                    length += readCount;
                }
            }
            MatrixMultiplier matrix = new MatrixMultiplier(FilePath);
            return matrix;
        }
        [WebMethod]
        public MatrixMultiplier getMatrixMultiplier(MatrixMultiplier mA, MatrixMultiplier mB)
        {
            MatrixMultiplier matrixC = new MatrixMultiplier();
            matrixC.body= matrixC.matrixMultiplier(mA, mB);
            return matrixC;
        }
    }
}
