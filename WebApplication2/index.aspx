﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApplication2.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        	<asp:FileUpload ID="FileUpload1" runat="server" />
			<asp:FileUpload ID="FileUpload2" runat="server" />
			<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        </div>
        <asp:Table ID="Table1" runat="server"></asp:Table>
        <br />
        <asp:Table ID="Table2" runat="server"></asp:Table>
        <br />
        <asp:Table ID="Table3" runat="server"></asp:Table>
    </form>
    <a href="mandelbrot.aspx">Mandelbrot</a>
</body>
</html>
