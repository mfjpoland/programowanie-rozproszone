﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication2
{
    /// <summary>
    /// Summary description for Mandelbrot1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Mandelbrot1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string Draw(int x, int y)
        {
            MemoryStream ms = new MemoryStream();
            Mandelbrot.Draw(x,y).Save(ms, ImageFormat.Bmp);
            var base64Data = Convert.ToBase64String(ms.ToArray());
            return  base64Data;

        }
    }
}
