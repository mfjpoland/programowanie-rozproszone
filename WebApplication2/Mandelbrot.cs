﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication2
{
    public class Mandelbrot
    {
        const double MaxValueExtent = 2.0;
       

        static double CalcMandelbrotSetColor(ComplexNumber c)
        {
            // from http://en.wikipedia.org/w/index.php?title=Mandelbrot_set
            const int MaxIterations = 1000;
            const double MaxNorm = MaxValueExtent * MaxValueExtent;

            int iteration = 0;
            ComplexNumber z = new ComplexNumber();
            do
            {
                z = z * z + c;
                iteration++;
            } while (z.Norm() < MaxNorm && iteration < MaxIterations);
            if (iteration < MaxIterations)
                return (double)iteration / MaxIterations;
            else
                return 0; // black
        }

        static void GenerateBitmap(Bitmap bitmap)
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
            int bytesPerPixel = Bitmap.GetPixelFormatSize(bitmap.PixelFormat) / 8;
            int byteCount = bitmapData.Stride * bitmap.Height;
        

            byte[] pixels = new byte[byteCount];
            IntPtr ptrFirstPixel = bitmapData.Scan0;
            Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
            int height = bitmapData.Height;
            int width = bitmapData.Width;
            int widthInBytes = bitmapData.Width * bytesPerPixel;

            double scale = 2 * MaxValueExtent / Math.Min(bitmap.Width, bitmap.Height);
            // goes through bitmap raw data parallelly
            Parallel.For(0, height, new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
            
            {
                int currentLine = i * bitmapData.Stride;
                double y = (height / 2 - i) * scale;
                for (int j = 0; j < widthInBytes; j+= bytesPerPixel)
                {
                    double x = ((j / bytesPerPixel) - width / 2) * scale;
                    double color = CalcMandelbrotSetColor(new ComplexNumber(x, y));
                    pixels[currentLine + j]=(byte)GetColor(color).R;
                    pixels[currentLine + j+1] = (byte)GetColor(color).G;
                    pixels[currentLine + j+2] = (byte)GetColor(color).B;
                }
            });
            Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);
            bitmap.UnlockBits(bitmapData);
        }

        static Color GetColor(double value)
        {
            const double MaxColor = 256;
            const double ContrastValue = 0.2;
            return Color.FromArgb(0, 0,
                (int)(MaxColor * Math.Pow(value, ContrastValue)));
        }

       
        public static Bitmap Draw(int x, int y)
        {
            // start from small image to provide instant display for user
            Size size= new Size();
            size.Width = x;
            size.Height = y;
            int width = 16;
            while (width * 2 < size.Width)
            {
                int height = width * size.Height / size.Width;
                Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                GenerateBitmap(bitmap);
                //this.BeginInvoke(new SetNewBitmapDelegate(SetNewBitmap), bitmap);
                width *= 2;
                //Thread.Sleep(200);
            }
            // then generate final image
            Bitmap finalBitmap = new Bitmap(size.Width, size.Height, PixelFormat.Format24bppRgb);
            GenerateBitmap(finalBitmap);
            return finalBitmap;
            //this.BeginInvoke(new SetNewBitmapDelegate(SetNewBitmap), finalBitmap);
        }

        void SetNewBitmap(Bitmap image, Bitmap background)
        {
            if (background != null)
                background.Dispose();
            background = image;
        }

        delegate void SetNewBitmapDelegate(Bitmap image);

       
    }
    struct ComplexNumber
    {
        public double Re;
        public double Im;

        public ComplexNumber(double re, double im)
        {
            this.Re = re;
            this.Im = im;
        }

        public static ComplexNumber operator +(ComplexNumber x, ComplexNumber y)
        {
            return new ComplexNumber(x.Re + y.Re, x.Im + y.Im);
        }

        public static ComplexNumber operator *(ComplexNumber x, ComplexNumber y)
        {
            return new ComplexNumber(x.Re * y.Re - x.Im * y.Im,
                x.Re * y.Im + x.Im * y.Re);
        }

        public double Norm()
        {
            return Re * Re + Im * Im;
        }
    }
}