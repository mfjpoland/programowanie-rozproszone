﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace WebApplication2
{
    [XmlType("Matrix")]
    public class MatrixMultiplier
    {
        public MatrixMultiplier(string path = null) {
            if (path != null)
            this.koadMatrix(path);
        }
        [XmlElement(Type = typeof(double[][]), ElementName = "Body")]

        public double[][] body { get; set; }
       
        [XmlElement(Type = typeof(int), ElementName = "x")]

        public int X { get; set; }
        [XmlElement(Type = typeof(int), ElementName = "y")]
        public int Y { get; set; }

        public void koadMatrix(string path)
        {
            List<List<double>> matrix = new List<List<double>>();
            StreamReader sr = new StreamReader(path);
            string txt = sr.ReadToEnd();
            sr.Close();
            string [] lines = Regex.Split(txt, "\r\n");
            if(lines.Length<=1)
            lines = Regex.Split(txt, "\n");
            int i = 0;
            foreach(string line in lines)
            {
                if (line != "")
                {
                    matrix.Add(new List<double>());
                   line.Replace('.', ',');
                    string[] values =line.Split(';');
                    foreach (string value in values)
                    {
                        matrix[i].Add(Convert.ToDouble(value));

                    }
                    this.X = matrix[i].Count();
                i++;
                }
            }
            this.Y = matrix.Count();
            this.body = matrix.Select(a => a.ToArray()).ToArray();
        }
        public MatrixMultiplier() { }
        public double[][] matrixMultiplier(MatrixMultiplier matrixA, MatrixMultiplier matrixB)
        {
            
            this.X =matrixA.X;
            this.Y= matrixB.Y;
            double[][] result = new double[matrixA.X][];
            if (matrixA.X == matrixB.Y)
            {
                Parallel.For(0, matrixA.X, i =>
                {
                // zmienna tymczasowa pozwoli na optymalizację równległych obliczeń
                result[i] = new double[matrixB.Y];
                    for (int j = 0; j < matrixB.Y; j++)
                    {

                        for (int k = 0; k < matrixA.Y; k++)
                        {
                            result[i][j] += matrixA.body[i][k] * matrixB.body[k][j];
                        }
                    }

                });
            }
            return result;
        }
    }
}