﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class mandelbrot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            MemoryStream ms = new MemoryStream();
            Mandelbrot.Draw(Convert.ToInt32(TextBox1.Text), Convert.ToInt32(TextBox2.Text)).Save(ms, ImageFormat.Bmp);
            var base64Data = Convert.ToBase64String(ms.ToArray());
            Image1.ImageUrl = "data:image;base64," +base64Data;
        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}