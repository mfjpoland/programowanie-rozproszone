﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace WebserviceClient
{
    public partial class Form1 : Form
    {
        MatrixMultiplier.Matrix matrixA, matrixB,matrixC;
        public Form1()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MatrixMultiplier.MatrixMultiplier1 m = new MatrixMultiplier.MatrixMultiplier1();
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(openFileDialog1.FileName, FileMode.Open, System.IO.FileAccess.Read))
                file.CopyTo(ms);
            string fileString = System.Text.Encoding.UTF8.GetString(ms.ToArray());
          matrixA=  m.setMatrix(openFileDialog1.FileName, fileString);
                       //m.Matrix(openFileDialog1.FileName, System.Text.Encoding.UTF8.GetString(ms.ToArray()));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MatrixMultiplier.MatrixMultiplier1 m = new MatrixMultiplier.MatrixMultiplier1();
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(openFileDialog1.FileName, FileMode.Open, System.IO.FileAccess.Read))
                file.CopyTo(ms);
            string fileString = System.Text.Encoding.UTF8.GetString(ms.ToArray());
            matrixB = m.setMatrix(openFileDialog1.FileName, fileString);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MatrixMultiplier.MatrixMultiplier1 m = new MatrixMultiplier.MatrixMultiplier1();
            matrixC = m.getMatrixMultiplier(matrixA, matrixB);
            dataGridView1.ColumnCount = matrixC.x;
            for (int i = 0; i < matrixC.x; i++)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                for (int j = 0; j < matrixC.y; j++)
                {
                    row.Cells[j].Value =  matrixC.Body[i][j];
                }
                dataGridView1.Rows.Add(row);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Mandelbrot.Mandelbrot1 mandelbrot = new Mandelbrot.Mandelbrot1();
            string base64= mandelbrot.Draw(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text));
            byte[] imageBytes = Convert.FromBase64String(base64);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                pictureBox1.Image = Image.FromStream(ms, true);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
